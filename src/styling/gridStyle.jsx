import styled from "styled-components";

export const Content = styled.div`
  width: 1199.98px;
  position: absolute;
  left: 0;
  right: 0;
  margin: 0 auto;

  @media (max-width: 1199.98px) {
    width: 991.98px;
  }

  @media (max-width: 991.98px) {
    width: 100%;
  }
`;
export const Row = styled.div`
  display: flex;
  flex-direction: row;
`;
export const Col = styled.div`
  flex: 0 0 ${props => props.size * 8.3333}%;
`;
