import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    body {
        @import url('https://fonts.googleapis.com/css?family=Fira+Sans:200,400,500,700,900i');
        font-family: 'Fira Sans', sans-serif;
        padding: 0;
        margin: 0;
        background: #353535;
    }
`;
