import React, { Component } from "react";
import styled from "styled-components";

import NavBar from "../components/NavBar";

import headerbg from "../assets/img/movie_example_header_bg.png";
import fade from "../assets/img/fade.png";

const HeaderWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  background: url(${headerbg});
  background-position: center;
  background-size: cover;
  position: relative;
`;

const Fade = styled.div`
  width: 100%;
  height: 270px;
  position: absolute;
  bottom: 0;
  background: url(${fade});
`;

class Header extends Component {
  render() {
    return (
      <div>
        <HeaderWrapper>
          <NavBar />
          <Fade />
        </HeaderWrapper>
      </div>
    );
  }
}

export default Header;
