import React, { Component } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

import { Content, Row, Col } from "../styling/gridStyle";

import logo from "../assets/img/mucs_white.png";

const NavHandler = styled.div`
  height: 100px;
  line-height: 100px;
`;

const Logo = styled.img`
  height: auto;
  width: 130px;
  position: absolute;
  top: 0;
  bottom: 0;
  margin: auto 0;
`;

const NavList = styled.ul`
  padding: 0;
  margin: 0;
  list-style: none;
`;

const NavItem = styled.li`
  padding: 0;
  margin: 0;
  display: inline-block;
  margin-right: 50px;
  color: white;
  font-weight: 200;

  &:last-child {
    margin-right: 0;
  }
`;

const NavLink = styled.link`
  &:after {
    content: "";
    position: absolute;
    left: 0;
    bottom: -7px;
    height: 4px;
    background-color: #fff;
    width: 0;
    transition: width 0.25s;
  }
`;

class NavBar extends Component {
  render() {
    return (
      <div>
        <Content>
          <NavHandler>
            <Row>
              <Col size="2">
                <Logo src={logo} />
              </Col>
              <Col size="6">
                <NavList>
                  <NavItem>
                    <Link
                      style={{ textDecoration: "none", color: "inherit" }}
                      to="/"
                    >
                      Início
                    </Link>
                  </NavItem>
                  <NavItem>
                    <Link
                      style={{ textDecoration: "none", color: "inherit" }}
                      to="/public"
                    >
                      Catálogo
                    </Link>
                  </NavItem>
                  <NavItem>
                    <Link
                      style={{ textDecoration: "none", color: "inherit" }}
                      to="/public"
                    >
                      Minha lista
                    </Link>
                  </NavItem>
                  <NavItem>
                    <Link
                      style={{ textDecoration: "none", color: "inherit" }}
                      to="/public"
                    >
                      Continuar assistindo
                    </Link>
                  </NavItem>
                </NavList>
              </Col>
            </Row>
          </NavHandler>
        </Content>
      </div>
    );
  }
}

export default NavBar;
