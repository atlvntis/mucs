import React, { Component } from "react";

import GlobalStyle from "../styling/globalStyle";

import Header from "../components/Header";

class Home extends Component {
  render() {
    return (
      <div>
        <GlobalStyle />
        <Header />
      </div>
    );
  }
}

export default Home;
